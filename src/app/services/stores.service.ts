import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@env/environment';

import { Stores } from '@app/models';

@Injectable({
  providedIn: 'root'
})
export class StoresService {
  URL = environment.server;

  constructor(private http: HttpClient) {}

  getAll(): Observable<Stores[]> {
    return this.http.get(`${this.URL}/api/stores`).pipe(
      map((stores: Stores[]) => {
        return stores;
      })
    );
  }
}
