import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@env/environment';

import { POI } from '@app/models';

@Injectable({
  providedIn: 'root'
})
export class PoiService {
  URL = environment.server;

  constructor(private http: HttpClient) {}

  getAll(query?: string): Observable<POI[]> {
    return this.http.get(`${this.URL}/api/poi`).pipe(
      map((records: POI[]) => {
        return records;
      })
    );
  }

  create(poi: POI): Observable<POI> {
    return this.http.post(`${this.URL}/api/poi`, poi).pipe(
      map((record: POI) => {
        return record;
      })
    );
  }

  update(poi: any): Observable<POI> {
    return this.http.patch(`${this.URL}/api/poi/${poi._id}`, poi).pipe(
      map((record: any) => {
        return record;
      })
    );
  }
}
