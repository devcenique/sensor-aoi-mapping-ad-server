import { AOI } from '@app/models/aoi.model';

export interface Stores {
  id: string;
  name: string;
  groupId: string;
  accountId: string;
  company: string;
  devices: AOI[];
}
