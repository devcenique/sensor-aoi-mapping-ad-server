export interface AOI {
  name: string; // AOI Name
  sensorCode: string;
  deviceType: string;
  sensorType: string;
  inputType: string;
  mac: string;
  channelName: string;
  channelInput: string;
  description: string;
  vid: string;
}
