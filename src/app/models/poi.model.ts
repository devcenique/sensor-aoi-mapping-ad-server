export interface POI {
  poi: string;
  name: string;
  description: string;
  customerId: string;
  vid: string;
}
