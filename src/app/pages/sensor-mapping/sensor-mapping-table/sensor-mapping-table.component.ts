import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { Store } from '@ngrx/store';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Observable, Subscription } from 'rxjs';

import * as fromStore from '@app/store';

import { AOI, Stores, POI } from '@app/models';

@Component({
  selector: 'app-sensor-mapping-table',
  templateUrl: './sensor-mapping-table.component.html',
  styleUrls: ['./sensor-mapping-table.component.scss']
})
export class SensorMappingTableComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  store$: Observable<Stores>;
  poi$: Observable<POI[]>;

  store_: Stores;
  poiList: POI[];

  records: AOI[] = [];
  poi: any;
  page = 1;

  modalRef: BsModalRef;

  masterPoi: POI = {
    name: '',
    poi: '',
    description: '',
    customerId: '',
    vid: ''
  };

  constructor(
    private store: Store<fromStore.State>,
    private modalService: BsModalService
  ) {
    this.store$ = this.store.select(fromStore.getStore);
    this.poi$ = this.store.select(fromStore.getPoiList);
  }

  showModel(template: TemplateRef<any>, vid: string) {
    if (vid) {
      const poi = this.poiList.find(p => p.vid === vid);
      if (poi) {
        this.poi = poi;
      } else {
        this.poi = { ...this.masterPoi };
        this.poi.customerId = this.store_.accountId;
        this.poi.vid = vid;
      }
      this.modalRef = this.modalService.show(template);
    }
  }

  submit(poi: any) {
    if (poi.name !== '' && poi.poi !== '') {
      if (poi._id) {
        this.store.dispatch(new fromStore.fromPoi.UpdatePOI(poi));
      } else {
        this.store.dispatch(new fromStore.fromPoi.CreateNewPOI(poi));
      }
      this.modalRef.hide();
    }
  }

  ngOnInit() {
    // this.refreshAoi();
    // this.store$.subscribe(store => {
    //   this.refreshAoi(store);
    // });
    this.store$.subscribe(store => {
      if (store) {
        this.store_ = store;
        this.records = store.devices;
      }
    });
    this.poi$.subscribe(poiList => {
      if (poiList) {
        this.poiList = poiList;
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
