import { Component, OnDestroy, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import { Subscription, Observable } from 'rxjs';

import * as fromStore from '@app/store';

@Component({
  selector: 'app-sensor-mapping',
  templateUrl: './sensor-mapping.component.html',
  styleUrls: ['./sensor-mapping.component.scss']
})
export class SensorMappingComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  loading$: Observable<boolean>;
  created$: Observable<boolean>;
  updated$: Observable<boolean>;

  constructor(private store: Store<fromStore.State>) {
    this.refresh();
    this.loading$ = this.store.select(fromStore.getStoresLoading);
    this.created$ = this.store.select(fromStore.getPoiCreated);
    this.updated$ = this.store.select(fromStore.getPoiUpdated);
  }

  refresh() {
    this.store.dispatch(new fromStore.fromStores.LoadStores());
    this.store.dispatch(new fromStore.fromPoi.LoadPOI());
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }

  ngOnInit() {
    this.created$.subscribe(created => {
      if (created) {
        this.refresh();
      }
    });
    this.updated$.subscribe(updated => {
      if (updated) {
        this.refresh();
      }
    });
  }
}
