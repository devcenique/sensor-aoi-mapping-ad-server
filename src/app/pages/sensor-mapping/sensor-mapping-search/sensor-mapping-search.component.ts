import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';

import { Observable, Subscription } from 'rxjs';

import * as fromStore from '@app/store';

import { Stores } from '@app/models';

@Component({
  selector: 'app-sensor-mapping-search',
  templateUrl: './sensor-mapping-search.component.html',
  styleUrls: ['./sensor-mapping-search.component.scss']
})
export class SensorMappingSearchComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  stores$: Observable<Stores[]>;
  loading$: Observable<boolean>;
  companies: any[] = [];
  storeId;

  constructor(private store: Store<fromStore.State>) {
    this.stores$ = this.store.select(fromStore.getAllStores);
    this.loading$ = this.store.select(fromStore.getStoresLoading);
  }

  ngOnInit() {
    this.stores$.subscribe(stores => {
      if (stores.length > 0) {
        this.refresh(stores);
      }
    });
  }

  refresh(stores: Stores[]) {
    this.companies = [];
    stores.forEach(store => {
      const company = this.companies.find(c => c.id === store.accountId);
      if (company) {
        company.stores.push(store);
      } else {
        this.companies.push({
          id: store.accountId,
          name: store.company,
          stores: [store]
        });
      }
    });
    if (this.storeId) {
      this.changeStore(stores.find(s => s.id === this.storeId));
    } else {
      this.changeStore(stores[0]);
    }
  }

  changeStore(store: Stores) {
    this.storeId = store.id;
    this.store.dispatch(new fromStore.fromStores.LoadStore(store));
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
