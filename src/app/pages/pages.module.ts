import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { NgxPaginationModule } from 'ngx-pagination';

import { ComponentsModule } from '@app/components/components.module';
import { PipesModule } from '@app/pipes/pipes.module';

import { SensorMappingComponent } from './sensor-mapping/sensor-mapping.component';
import { SensorMappingTableComponent } from './sensor-mapping/sensor-mapping-table/sensor-mapping-table.component';
import { SensorMappingSearchComponent } from './sensor-mapping/sensor-mapping-search/sensor-mapping-search.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    PipesModule,
    ComponentsModule
  ],
  declarations: [
    SensorMappingComponent,
    SensorMappingTableComponent,
    SensorMappingSearchComponent
  ]
})
export class PagesModule {}
