import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatProgressSpinnerModule } from '@angular/material';

import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  imports: [CommonModule, MatProgressSpinnerModule],
  exports: [NavbarComponent, MatProgressSpinnerModule],
  declarations: [NavbarComponent]
})
export class ComponentsModule {}
