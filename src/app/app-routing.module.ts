import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SensorMappingComponent } from '@app/pages/sensor-mapping/sensor-mapping.component';

const routes: Routes = [
  {
    path: 'sensor-mapping',
    component: SensorMappingComponent
    // canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/sensor-mapping',
    pathMatch: 'full'
    // canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
