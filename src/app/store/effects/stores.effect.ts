import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { StoresService } from '@app/services';
import { fromStores } from '@app/store/actions';
import { Stores } from '@app/models';

@Injectable()
export class StoresEffect {
  constructor(
    private actions$: Actions,
    private storesService: StoresService
  ) {}

  @Effect()
  getStores$ = this.actions$.ofType(fromStores.LOAD_STORES).pipe(
    switchMap(() => {
      return this.storesService.getAll().pipe(
        map((records: Stores[]) => new fromStores.LoadStoresSuccess(records)),
        catchError(err => of(new fromStores.LoadStoresFail(err)))
      );
    })
  );
}
