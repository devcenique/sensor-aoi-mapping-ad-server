import { PoiEffect } from '@app/store/effects/poi.effect';
import { StoresEffect } from '@app/store/effects/stores.effect';

export const effects = [PoiEffect, StoresEffect];
