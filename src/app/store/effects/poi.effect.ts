import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { PoiService } from '@app/services';
import { fromPoi } from '@app/store/actions';
import { POI } from '@app/models';

@Injectable()
export class PoiEffect {
  constructor(private actions$: Actions, private poiService: PoiService) {}

  @Effect()
  getPOI$ = this.actions$.ofType(fromPoi.LOAD_POI).pipe(
    switchMap((state: fromPoi.LoadPOI) => {
      return this.poiService.getAll(state.payload).pipe(
        map((records: POI[]) => new fromPoi.LoadPOISuccess(records)),
        catchError(err => of(new fromPoi.LoadPOIFail(err)))
      );
    })
  );

  @Effect()
  createPOI$ = this.actions$.ofType(fromPoi.CREATE_POI).pipe(
    switchMap((state: fromPoi.CreateNewPOI) => {
      return this.poiService.create(state.payload).pipe(
        map((record: POI) => new fromPoi.CreateNewPOISuccess(record)),
        catchError(err => of(new fromPoi.CreateNewPOIFail(err)))
      );
    })
  );

  @Effect()
  updatePOI$ = this.actions$.ofType(fromPoi.UPDATE_POI).pipe(
    switchMap((state: fromPoi.UpdatePOI) => {
      return this.poiService.update(state.payload).pipe(
        map((record: any) => new fromPoi.UpdatePOISuccess(record)),
        catchError(err => of(new fromPoi.UpdatePOIFail(err)))
      );
    })
  );
}
