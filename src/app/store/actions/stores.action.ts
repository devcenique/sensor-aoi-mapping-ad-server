import { Action } from '@ngrx/store';
import { Stores } from '@app/models';

export const LOAD_STORES = '[Stores] LOAD';
export const LOAD_STORES_SUCCESS = '[Stores] LOAD SUCCESS';
export const LOAD_STORES_FAIL = '[Stores] LOAD FAIL';
export const LOAD_STORE = '[Store] LOAD';

export class LoadStores implements Action {
  readonly type = LOAD_STORES;
  constructor(public payload?: string) {}
}

export class LoadStoresSuccess implements Action {
  readonly type = LOAD_STORES_SUCCESS;
  constructor(public payload: Stores[]) {}
}

export class LoadStoresFail implements Action {
  readonly type = LOAD_STORES_FAIL;
  constructor(public payload: any) {}
}

export class LoadStore implements Action {
  readonly type = LOAD_STORE;
  constructor(public payload: Stores) {}
}

export type StoresAction =
  | LoadStores
  | LoadStoresSuccess
  | LoadStoresFail
  | LoadStore;
