import { Action } from '@ngrx/store';
import { POI } from '@app/models';

export const LOAD_POI = '[POI] LOAD';
export const LOAD_POI_SUCCESS = '[POI] LOAD SUCCESS';
export const LOAD_POI_FAIL = '[POI] LOAD FAIL';
export const CREATE_POI = '[POI] CREATE';
export const CREATE_POI_SUCCESS = '[POI] CREATE SUCCESS';
export const CREATE_POI_FAIL = '[POI] CREATE FAIL';
export const UPDATE_POI = '[POI] UPDATE';
export const UPDATE_POI_SUCCESS = '[POI] UPDATE SUCCESS';
export const UPDATE_POI_FAIL = '[POI] UPDATE FAIL';

export class LoadPOI implements Action {
  readonly type = LOAD_POI;
  constructor(public payload?: string) {}
}

export class LoadPOISuccess implements Action {
  readonly type = LOAD_POI_SUCCESS;
  constructor(public payload: POI[]) {}
}

export class LoadPOIFail implements Action {
  readonly type = LOAD_POI_FAIL;
  constructor(public payload: any) {}
}

export class CreateNewPOI implements Action {
  readonly type = CREATE_POI;
  constructor(public payload: POI) {}
}

export class CreateNewPOISuccess implements Action {
  readonly type = CREATE_POI_SUCCESS;
  constructor(public payload: POI) {}
}

export class CreateNewPOIFail implements Action {
  readonly type = CREATE_POI_FAIL;
  constructor(public payload: any) {}
}

export class UpdatePOI implements Action {
  readonly type = UPDATE_POI;
  constructor(public payload: any) {}
}

export class UpdatePOISuccess implements Action {
  readonly type = UPDATE_POI_SUCCESS;
  constructor(public payload: any) {}
}

export class UpdatePOIFail implements Action {
  readonly type = UPDATE_POI_FAIL;
  constructor(public payload: any) {}
}

export type PoiAction =
  | LoadPOI
  | LoadPOISuccess
  | LoadPOIFail
  | CreateNewPOI
  | CreateNewPOISuccess
  | CreateNewPOIFail
  | UpdatePOI
  | UpdatePOISuccess
  | UpdatePOIFail;
