import * as fromPoi from '@app/store/actions/poi.action';
import * as fromStores from './stores.action';

export { fromPoi, fromStores };
