import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { fromStores } from '@app/store/actions';
import { Stores } from '@app/models';

export interface StoresState extends EntityState<Stores> {
  stores: Stores[];
  store: Stores;
  storesLoading: boolean;
}

export const StoresAdapter: EntityAdapter<Stores> = createEntityAdapter<
  Stores
>();

const initialState: StoresState = StoresAdapter.getInitialState({
  stores: [],
  store: undefined,
  storesLoading: false
});

export function StoresReducer(
  state: StoresState = initialState,
  action: fromStores.StoresAction
): StoresState {
  switch (action.type) {
    case fromStores.LOAD_STORES:
      return {
        ...state,
        storesLoading: true
      };
    case fromStores.LOAD_STORES_SUCCESS:
      return {
        ...state,
        stores: action.payload,
        storesLoading: false
      };
    case fromStores.LOAD_STORES_FAIL:
      return {
        ...state,
        stores: action.payload,
        storesLoading: false
      };
    case fromStores.LOAD_STORE:
      return {
        ...state,
        store: action.payload
      };
    default:
      return state;
  }
}

export const getStoresRecords = (state: StoresState) => {
  return state.stores;
};

export const getStoresLoading = (state: StoresState) => {
  return state.storesLoading;
};

export const getStoreRecords = (state: StoresState) => {
  return state.store;
};
