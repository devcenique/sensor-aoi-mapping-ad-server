import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { fromPoi } from '@app/store/actions';
import { POI } from '@app/models';

export interface PoiState extends EntityState<POI> {
  poi: POI[];
  query: string;
  create: POI;
  created: boolean;
  update: any;
  updated: boolean;
}

export const poiAdapter: EntityAdapter<POI> = createEntityAdapter<POI>();

const initialState: PoiState = poiAdapter.getInitialState({
  poi: [],
  query: '',
  create: undefined,
  created: false,
  update: undefined,
  updated: false
});

export function poiReducer(
  state: PoiState = initialState,
  action: fromPoi.PoiAction
): PoiState {
  switch (action.type) {
    case fromPoi.LOAD_POI:
      return {
        ...state,
        query: action.payload
      };
    case fromPoi.LOAD_POI_SUCCESS:
      return {
        ...state,
        poi: action.payload
      };
    case fromPoi.LOAD_POI_FAIL:
      return {
        ...state,
        poi: action.payload
      };
    case fromPoi.CREATE_POI: {
      return {
        ...state,
        create: action.payload
      };
    }
    case fromPoi.CREATE_POI_SUCCESS: {
      return {
        ...state,
        create: action.payload,
        created: true
      };
    }
    case fromPoi.CREATE_POI_FAIL: {
      return {
        ...state,
        create: action.payload,
        created: false
      };
    }
    case fromPoi.UPDATE_POI: {
      return {
        ...state,
        update: action.payload
      };
    }
    case fromPoi.UPDATE_POI_SUCCESS: {
      return {
        ...state,
        updated: true
      };
    }
    case fromPoi.UPDATE_POI_FAIL: {
      return {
        ...state,
        updated: false
      };
    }
    default:
      return state;
  }
}

export const getPoiRecords = (state: PoiState) => {
  return state.poi;
};

export const getCreateSuccessful = (state: PoiState) => {
  return state.created;
};

export const getUpdateSuccessful = (state: PoiState) => {
  return state.updated;
};
