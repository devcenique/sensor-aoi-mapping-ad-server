import { ActionReducerMap, createSelector } from '@ngrx/store';

import * as fromPoi from '@app/store/reducers/poi.reducer';
import * as fromStores from './stores.reducer';

export interface State {
  poi: fromPoi.PoiState;
  stores: fromStores.StoresState;
}

export const reducers: ActionReducerMap<State> = {
  poi: fromPoi.poiReducer,
  stores: fromStores.StoresReducer
};

export const getPoiState = (state: State) => state.poi;

export const getPoiList = createSelector(getPoiState, fromPoi.getPoiRecords);

export const getPoiUpdated = createSelector(
  getPoiState,
  fromPoi.getUpdateSuccessful
);

export const getPoiCreated = createSelector(
  getPoiState,
  fromPoi.getCreateSuccessful
);

export const getStoresState = (state: State) => state.stores;

export const getAllStores = createSelector(
  getStoresState,
  fromStores.getStoresRecords
);

export const getStore = createSelector(
  getStoresState,
  fromStores.getStoreRecords
);

export const getStoresLoading = createSelector(
  getStoresState,
  fromStores.getStoresLoading
);
